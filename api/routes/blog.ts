import { Response, Router } from "express";
import { StatusCodes } from "http-status-codes";
import { checkAuth } from "../middlewares/checkAuth";
import { requireAuth } from "../middlewares/requireAuth";
import { BlogModel, blogValidator } from "../models/Blog";
import { Roles } from "../models/user";
import { AuthenticatedRequest } from "../types/CustomRequest";

const router = Router();

(router as any).get(
  "/",
  checkAuth,
  async (req: AuthenticatedRequest, res: Response) => {
    let blogs;

    if (req.query.all) {
      if (req.user && req.user.role === Roles.Admin) {
        blogs = await BlogModel.find().populate("user", "name email");
      } else {
        blogs = await BlogModel.find({ approved: true }).populate(
          "user",
          "name email"
        );
      }
    } else {
      blogs = await BlogModel.find({ approved: true }).populate(
        "user",
        "name email"
      );
    }

    // TODO: pagination
    res.status(StatusCodes.OK).json({
      ok: true,
      offset: 0,
      total: blogs.length,
      blogs,
    });
  }
);

router.get("/:id", async (req, res) => {
  const blog = await BlogModel.findById(req.params.id)
    .populate("user", "name email")
    .exec();

  res.status(StatusCodes.OK).json({
    ok: true,
    blog,
  });
});

(router as any).post(
  "/",
  requireAuth,
  async (req: AuthenticatedRequest, res: Response) => {
    const { error, value } = blogValidator.validate(req.body);

    if (error) {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .json({ ok: false, message: error.message });
    }

    if ([Roles.Admin, Roles.Writer].includes(req.user!.role)) {
      const blog = new BlogModel({
        title: value.title,
        body: value.body,
        user: req.user!.id,
        approved: req.user!.role === Roles.Admin ? true : false,
      });

      await blog.save();
      return res.status(StatusCodes.CREATED).json({
        ok: true,
        message: "blog created",
        blog: {
          id: blog.id,
          title: blog.title,
          body: blog.body,
          user: {
            name: req.user!.name,
            email: req.user!.email,
          },
        },
      });
    } else {
      return res.status(StatusCodes.BAD_REQUEST).json({
        ok: false,
        message: "Only ADMINs or WRITERs can create blogs",
      });
    }
  }
);

(router as any).post(
  "/approve/:id",
  requireAuth,
  async (req: AuthenticatedRequest, res: Response) => {
    const { disapprove } = req.body;

    if (req.user!.role !== Roles.Admin) {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .json({ ok: false, message: "Only ADMINs can approve or reject" });
    }

    await BlogModel.updateOne(
      { _id: req.params.id },
      { approved: disapprove ? false : true }
    );

    return res.status(StatusCodes.OK).json({
      ok: true,
      message: "blog approved",
    });
  }
);

export default router;
