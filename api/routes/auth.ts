import { Response, Router } from "express";
import bcrypt from "bcrypt";
import { StatusCodes } from "http-status-codes";
import {
  UserModel,
  signupValidator,
  loginValidator,
  Roles,
} from "../models/user";
import { createToken } from "../utils/createToken";
import { requireAuth } from "../middlewares/requireAuth";
import { AuthenticatedRequest } from "../types/CustomRequest";

const router = Router();

router.post("/signup", async (req, res) => {
  const { error, value: validatedUser } = signupValidator.validate(req.body);

  if (error) {
    return res
      .status(StatusCodes.BAD_REQUEST)
      .json({ ok: false, message: error.message });
  }

  // check if user exists
  const existingUser = await UserModel.findOne({
    email: validatedUser.email,
  }).exec();

  if (existingUser) {
    return res
      .status(StatusCodes.BAD_REQUEST)
      .json({ ok: false, message: "User with this email already exists" });
  }

  validatedUser.password = await bcrypt.hash(validatedUser.password, 10);
  const user = new UserModel(validatedUser);
  await user.save();

  return res.status(StatusCodes.CREATED).json({
    ok: true,
    message: "successfully created the user",
    userDoc: {
      id: user.id,
      name: user.name,
      email: user.email,
      role: user.role,
    },
  });
});

router.post("/login", async (req, res) => {
  const { error, value } = loginValidator.validate(req.body);

  if (error) {
    return res
      .status(StatusCodes.BAD_REQUEST)
      .json({ ok: false, message: error.message });
  }

  // check if user exists
  const user = await UserModel.findOne({ email: value.email }).exec();

  if (!user) {
    return res
      .status(StatusCodes.BAD_REQUEST)
      .json({ ok: false, message: "wrong credentials" });
  }

  // compare password
  if (await bcrypt.compare(value.password, user.password)) {
    return res
      .status(StatusCodes.OK)
      .json({ ok: true, token: createToken(user) });
  } else {
    return res
      .status(StatusCodes.BAD_REQUEST)
      .json({ ok: false, message: "wrong credentials" });
  }
});

(router as any).get(
  "/me",
  requireAuth,
  async (req: AuthenticatedRequest, res: Response) => {
    if (!req.user) {
      return res
        .status(StatusCodes.UNAUTHORIZED)
        .json({ ok: false, message: "Unauthorized" });
    }

    return res.status(StatusCodes.OK).json({
      ...req.user,
      id: "protected",
    });
  }
);

(router as any).get(
  "/users",
  requireAuth,
  async (req: AuthenticatedRequest, res: Response) => {
    if (req.user!.role === Roles.Admin) {
      const users = await UserModel.find().exec();
      res.status(StatusCodes.OK).json({
        ok: true,
        offset: 0,
        total: users.length,
        users: users.reverse().map((u) => ({
          name: u.name,
          email: u.email,
          role: u.role,
          id: u._id,
        })),
      });
    } else {
      res.status(StatusCodes.UNAUTHORIZED).json({
        ok: false,
        message: "You are not allowed to access this resource",
      });
    }
  }
);

export default router;
