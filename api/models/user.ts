import Joi from "joi";
import { model, Schema } from "mongoose";

export enum Roles {
  Admin = "ADMIN",
  Writer = "WRITER",
}

export interface User {
  name: string;
  email: string;
  password: string;
  role: Roles;
}

const userSchema = new Schema<User>({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  role: { type: String, default: Roles.Writer },
  approved: { type: Boolean, default: false },
  blogs: { type: Schema.Types.ObjectId, ref: "Blog" },
});

const commonValidator = {
  email: Joi.string().email().required(),
  password: Joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,30}$")).required(),
};

export const signupValidator = Joi.object<User>({
  name: Joi.string().min(1).required(),
  ...commonValidator,
});

export const loginValidator = Joi.object<User>(commonValidator);

export const UserModel = model<User>("User", userSchema, "users");
