import Joi from "joi";
import { model, Schema } from "mongoose";
import { User } from "./user";

export interface Blog {
  title: string;
  body: string;
  user: User;
  approved: boolean;
}

const blogSchema = new Schema<Blog>({
  title: { type: String, required: true },
  body: { type: String, required: true },
  approved: { type: Boolean, required: true },
  user: { type: Schema.Types.ObjectId, ref: "User" },
});

export const blogValidator = Joi.object<Blog>({
  title: Joi.string().required(),
  body: Joi.string().required(),
});

export const BlogModel = model("Blog", blogSchema, "blogs");
