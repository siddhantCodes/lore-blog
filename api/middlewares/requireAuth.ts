import { NextFunction, Response } from "express";
import { StatusCodes } from "http-status-codes";
import jwt from "jsonwebtoken";
import { AuthenticatedRequest, UserPayload } from "../types/CustomRequest";

export function requireAuth(
  req: AuthenticatedRequest,
  res: Response,
  next: NextFunction
) {
  try {
    const token = (req.headers as any)["authorization"]?.split(" ")[1];

    if (!token) {
      throw new Error("Unauthorized");
    } else {
      req.user = jwt.verify(token, process.env.SECRET!) as UserPayload;
    }
    return next();
  } catch (error) {
    res
      .status(StatusCodes.UNAUTHORIZED)
      .json({ ok: false, message: error.message || "Unauthorized" });
  }
}
