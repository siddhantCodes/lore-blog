import { NextFunction } from "express";
import jwt from "jsonwebtoken";
import { AuthenticatedRequest, UserPayload } from "../types/CustomRequest";

export const checkAuth = (
  req: AuthenticatedRequest,
  _: Response,
  next: NextFunction
) => {
  try {
    const token = (req.headers as any)["authorization"]?.split(" ")[1];

    if (!token) {
      req.user = null;
    } else {
      req.user = jwt.verify(token, process.env.SECRET!) as UserPayload;
    }
    next();
    return;
  } catch (error) {
    next();
    return;
  }
};
