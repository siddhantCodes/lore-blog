import jwt from "jsonwebtoken";
import { Document } from "mongoose";
import { User } from "../models/user";

export function createToken({ name, email, role, id }: User & Document) {
  return jwt.sign({ name, email, role, id }, process.env.SECRET!, {
    expiresIn: "7d",
  });
}
