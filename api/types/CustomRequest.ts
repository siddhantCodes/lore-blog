import { Request } from "express";
import { User } from "../models/user";

export type UserPayload = Pick<User, "email" | "name" | "role"> & {
  id: string;
};

export interface AuthenticatedRequest extends Request {
  user: UserPayload | null;
}
