import { decode as jwtDecode } from "jsonwebtoken";
import { useEffect, useState } from "react";
import { API_URL } from "../constants";

export interface User {
  id: string;
  email: string;
  name: string;
  role: Roles;
}

export enum Roles {
  Admin = "ADMIN",
  Writer = "WRITER",
}

export const useUser = () => {
  const [user, setUser] = useState<User | null>();
  const [error, setError] = useState<string>();

  useEffect(() => {
    if (!user) {
      const token = localStorage.getItem("token");

      if (token) {
        try {
          setUser(jwtDecode(token) as User);
        } catch (error) {
          console.log(error);
        }
      }
    }
  }, [user]);

  const login = async (loginData: { email: string; password: string }) => {
    if (user) return true;
    const token = localStorage.getItem("token");

    if (token) {
      try {
        setUser(jwtDecode(token) as User);
      } catch (error) {
        console.log(error);
      }
      return true;
    } else {
      const res = await fetch(`${API_URL}/api/auth/login`, {
        method: "POST",
        body: JSON.stringify(loginData),
        headers: {
          "Content-Type": "application/json",
        },
      }).then((r) => r.json());

      if (!res.ok) setError(res.message);
      else {
        localStorage.setItem("token", res.token);
        // refresh the page and let the useEffect restore the user
        window.location.reload();
      }
    }
  };

  const logout = () => {
    localStorage.removeItem("token");
    window.location.reload();
    return true;
  };

  return {
    login,
    logout,
    user,
    error,
  };
};
