import React, { Dispatch, SetStateAction, useState } from "react";
import { CreatePost } from "../../Components/CreatePost";
import { ManageUsers } from "../../Components/ManageUsers";
import { Moderate } from "../../Components/Moderate";
import { Roles, useUser } from "../../hooks/useUser";
import { Blog } from "../Blogs";

export type AdminBlog = Blog & { approved: boolean };

enum Views {
  Admin = "ADMIN",
  Post = "WRITE_POST",
  Users = "USERS",
}

export const AdminPage = () => {
  const { user } = useUser();
  const [view, setView] = useState<Views>(Views.Admin);

  const renderView = () => {
    switch (view) {
      case Views.Admin:
        return <Moderate />;
      case Views.Post:
        return <CreatePost />;
      case Views.Users:
        return <ManageUsers />;
      default:
        return <Moderate />;
    }
  };

  return (
    <>
      <h1 className="mb-8 text-4xl">Admin</h1>

      {!user && ![Roles.Admin, Roles.Writer].includes((user as any)?.role) ? (
        <h2>You are not an Admin!</h2>
      ) : (
        <div className="">
          <Menu view={view} changeView={setView} />
          {renderView()}
        </div>
      )}
    </>
  );
};

function Menu({
  view,
  changeView,
}: {
  view: Views | undefined;
  changeView: Dispatch<SetStateAction<Views>>;
}) {
  const { user } = useUser();

  return (
    <div className="flex justify-center mb-6">
      <Tile
        active={view === Views.Admin}
        activate={() => changeView(Views.Admin)}
      >
        All Blogs
      </Tile>
      <Tile
        active={view === Views.Post}
        activate={() => changeView(Views.Post)}
      >
        Write a Blog
      </Tile>
      {user?.role === Roles.Admin && (
        <Tile
          active={view === Views.Users}
          activate={() => changeView(Views.Users)}
        >
          Users
        </Tile>
      )}
    </div>
  );
}

function Tile({
  children,
  activate,
  active,
}: {
  children: React.ReactNode;
  activate: () => void;
  active: boolean;
}) {
  return (
    <div
      onClick={activate}
      className={`p-1 cursor-pointer mr-2 ${
        active ? "border-0 border-b-4 border-blue-500" : ""
      } rounded`}
    >
      {children}
    </div>
  );
}
