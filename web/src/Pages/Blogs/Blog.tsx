import { useEffect, useState } from "react";
import { useParams } from "react-router";
import { Blog } from ".";
import { API_URL } from "../../constants";

export const BlogPage = () => {
  const { blogId } = useParams<{ blogId: string }>();
  const [blog, setBlog] = useState<Blog>();
  const [error, setError] = useState<string>();

  useEffect(() => {
    fetch(`${API_URL}/api/blogs/${blogId}`)
      .then((r) => r.json())
      .then((res) => {
        if (res.ok) setBlog(res.blog);
        else setError(res.message || "No blog with the provided found.");
      });
  }, [blogId]);

  if (error || !blog) {
    return <h1 className="text-4xl">{error}</h1>;
  }

  return (
    <>
      <h1 className="mb-8 text-4xl">{blog.title}</h1>

      <div className="text-left">
        <p className="text-white text-sm mb-2 ">{blog.body}</p>
        <p className="text-gray-300 text-sm">
          By {blog.user.name}
          {`<${blog.user.email}>`}
        </p>
      </div>
    </>
  );
};
