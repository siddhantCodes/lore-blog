import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Card } from "../../Components/Card";
import { API_URL } from "../../constants";
import { User } from "../../hooks/useUser";

export interface Blog {
  _id: string;
  title: string;
  body: string;
  user: Pick<User, "name" | "email">;
}

export const BlogsPage = () => {
  const [blogs, setBlogs] = useState<Array<Blog>>([]);
  const history = useHistory();

  useEffect(() => {
    fetch(`${API_URL}/api/blogs`)
      .then((r) => r.json())
      .then((res) => {
        if (res.ok) setBlogs(res.blogs);
      });
  }, []);

  const openBlog = (blog: Blog) => {
    history.push(`/${blog._id}`);
  };

  return (
    <>
      <h1 className="mb-8 text-4xl">Blogs</h1>

      {blogs.map((blog) => {
        return (
          <Card blog={blog} onClick={() => openBlog(blog)} key={blog._id} />
        );
      })}
      <br />
    </>
  );
};
