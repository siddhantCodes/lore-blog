import { FormEventHandler, useState } from "react";
import { Redirect } from "react-router-dom";
import { useUser } from "../../hooks/useUser";

export const LoginPage = () => {
  const [state, setState] = useState({ email: "", password: "" });
  const { login, user, error } = useUser();

  const handlelogin: FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();
    login(state);
  };

  return (
    <>
      <div className="w-full max-w-xs mx-auto mt-16">
        {user && <Redirect to="/" />}
        <form
          onSubmit={handlelogin}
          className="bg-gray-700 rounded px-8 pt-6 pb-8 mb-4"
        >
          <div className="mb-4">
            <label className="block text-sm font-semibold mb-2" htmlFor="email">
              Email
            </label>
            <input
              className="bg-gray-100 appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="email"
              type="email"
              placeholder="user@example.com"
              value={state.email}
              onChange={(e) => setState({ ...state, email: e.target.value })}
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-sm font-semibold mb-2"
              htmlFor="password"
            >
              password
            </label>
            <input
              className="bg-gray-100 appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="password"
              type="password"
              placeholder="*************"
              value={state.password}
              onChange={(e) => setState({ ...state, password: e.target.value })}
            />
          </div>
          {error && <p className="text-red-400 mb-2">{error}</p>}
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="submit"
          >
            Login
          </button>
        </form>
        <p className="text-center text-gray-500 text-xs">
          &copy;2021 LoreBlog. All rights reserved.
        </p>
      </div>
    </>
  );
};
