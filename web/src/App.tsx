import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import { Navbar } from "./Components/Navbar";
import { AdminPage } from "./Pages/Admin";
import { BlogsPage } from "./Pages/Blogs";
import { BlogPage } from "./Pages/Blogs/Blog";
import { LoginPage } from "./Pages/Login";
import { UsersPage } from "./Pages/Users";

function App() {
  return (
    <Router>
      <Navbar />
      <br />
      <Switch>
        <Route path="/login">
          <LoginPage />
        </Route>
        <Route path="/admin/users/new">
          <UsersPage />
        </Route>
        <Route path="/admin">
          <AdminPage />
        </Route>
        <Route path="/:blogId">
          <BlogPage />
        </Route>
        <Route path="/blog">
          <Redirect to="/" />
        </Route>
        <Route path="/">
          <BlogsPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
