import React from "react";
import { Roles, User } from "../../hooks/useUser";
import { Blog } from "../../Pages/Blogs";

export const Card = ({
  onClick,
  blog,
}: {
  onClick: React.MouseEventHandler<HTMLDivElement>;
  blog: Blog;
}) => {
  return (
    <div
      onClick={onClick}
      className="shadow p-4 bg-gray-700 rounded mb-4 cursor-pointer"
    >
      <div className="text-left">
        <h3 className="mb-2 text-xl font-bold">{blog.title}</h3>
        <p className="text-white text-sm mb-2">
          {blog.body.slice(0, 140)}
          {blog.body.length > 120 && "..."}
        </p>
        <p className="text-gray-300 text-sm">
          By {blog.user.name}
          {`<${blog.user.email}>`}
        </p>
      </div>
    </div>
  );
};

export const UserCard = ({ user }: { user: User }) => {
  const Tag = ({ role }: { role: Roles }) => {
    let classes = "rounded-md text-white p-2 text-xs";
    if (role === Roles.Admin) classes += " bg-green-400";
    else classes += " bg-gray-600";

    return <span className={classes}>{role.toLowerCase()}</span>;
  };

  return (
    <div className="shadow p-4 bg-gray-700 rounded mb-4 cursor-pointer">
      <div className="text-left">
        <h3 className="mb-2 text-xl font-bold">{user.name}</h3>
        <p className="text-white text-sm mb-2">{user.email}</p>
        <Tag role={user.role} />
      </div>
    </div>
  );
};

export const ApproveCard = ({
  blog,
  approve,
  reject,
}: {
  approve?: () => void;
  reject?: () => void;
  blog: Blog;
}) => {
  return (
    <div className="shadow p-4 bg-gray-700 rounded mb-4 cursor-pointer">
      <div className="text-left">
        <h3 className="mb-2 text-xl font-bold">{blog.title}</h3>
        <p className="text-white text-sm mb-2">
          {blog.body.slice(0, 140)}
          {blog.body.length > 120 && "..."}
        </p>
        <p className="text-gray-300 text-sm">
          By {blog.user.name}
          {`<${blog.user.email}>`}
        </p>
      </div>
      <div className="mt-4">
        {approve && (
          <button
            onClick={approve}
            className="no-underline mr-4 p-2 rounded bg-green-500 hover:bg-green-700"
          >
            Approve
          </button>
        )}
        {reject && (
          <button
            onClick={reject}
            className="no-underline mr-4 bg-red-500 p-2 rounded hover:bg-red-700"
          >
            Reject
          </button>
        )}
      </div>
    </div>
  );
};
