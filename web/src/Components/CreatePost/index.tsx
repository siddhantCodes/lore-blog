import { useState } from "react";
import { API_URL } from "../../constants";

export function CreatePost() {
  const [state, setState] = useState({ title: "", body: "" });

  const createPostHandler: React.FormEventHandler<HTMLFormElement> =
    async () => {
      const res = await fetch(`${API_URL}/api/blogs`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
        },
        body: JSON.stringify(state),
      }).then((r) => r.json());

      // this, for now, is the easies way to invalidate existing cache
      // and download fresh data from the api
      if (res.ok) {
        window.location.reload();
      }
    };

  return (
    <div className="flex">
      <div className="flex-1 p-2">
        <h3 className="text-xl font-bold mb-8">Write a Blog!</h3>
        <form
          onSubmit={createPostHandler}
          className="bg-gray-700 rounded px-8 pt-6 pb-8 mb-4"
        >
          <div className="mb-4">
            <label className="block text-sm font-semibold mb-2" htmlFor="title">
              Title
            </label>
            <input
              className="bg-gray-100 appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="title"
              type="text"
              placeholder="Blog Title..."
              value={state.title}
              onChange={(e) => setState({ ...state, title: e.target.value })}
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-sm font-semibold mb-2"
              htmlFor="content"
            >
              Content
            </label>
            <textarea
              rows={15}
              cols={50}
              className="bg-gray-100 p-1 appearance-none border rounded w-full text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="content"
              placeholder="Body of the post..."
              value={state.body}
              onChange={(e) => setState({ ...state, body: e.target.value })}
            />
          </div>

          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="submit"
          >
            Create post
          </button>
        </form>
      </div>
    </div>
  );
}
