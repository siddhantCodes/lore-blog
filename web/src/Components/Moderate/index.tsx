import { useState, useEffect } from "react";
import { API_URL } from "../../constants";
import { useUser, Roles } from "../../hooks/useUser";
import { AdminBlog } from "../../Pages/Admin";
import { ApproveCard } from "../Card";

export function Moderate() {
  const { user } = useUser();
  const [allBlogs, setAllBlogs] = useState<Array<AdminBlog>>([]);
  const unapproved = allBlogs.filter((b) => !b.approved);
  const approved = allBlogs.filter((b) => b.approved);

  const handleModeration = async (blog: AdminBlog, disapprove: boolean) => {
    const res = await fetch(`${API_URL}/api/blogs/approve/${blog._id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
      body: JSON.stringify({ disapprove }),
    }).then((r) => r.json());

    // this, for now, is the easies way to invalidate existing cache
    // and download fresh data from the api
    if (res.ok) window.location.reload();
  };

  useEffect(() => {
    fetch(`${API_URL}/api/blogs?all=true`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((r) => r.json())
      .then((res) => {
        if (res.ok) setAllBlogs(res.blogs);
      });
  }, []);

  return (
    <div className="flex">
      {user?.role === Roles.Admin && (
        <div className="flex-1 p-2">
          <h3 className="text-xl font-bold mb-8">Unapproved Blogs</h3>
          {unapproved.length ? (
            unapproved.map((blog) => {
              return (
                <ApproveCard
                  key={blog._id}
                  blog={blog}
                  approve={() => handleModeration(blog, false)}
                />
              );
            })
          ) : (
            <p>No Unapproved Blogs!</p>
          )}
        </div>
      )}
      <div className="flex-1 p-2">
        <h3 className="text-xl font-bold mb-8">Approved Blogs</h3>
        {approved.length ? (
          approved.map((blog) => {
            if (user?.role === Roles.Admin)
              return (
                <ApproveCard
                  key={blog._id}
                  blog={blog}
                  reject={() => handleModeration(blog, true)}
                />
              );
            else return <ApproveCard key={blog._id} blog={blog} />;
          })
        ) : (
          <p>No Approved Blogs!</p>
        )}
      </div>
    </div>
  );
}
