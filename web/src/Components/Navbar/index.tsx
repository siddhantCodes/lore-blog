import { Link, useLocation } from "react-router-dom";
import { Roles, useUser } from "../../hooks/useUser";

export const Navbar = () => {
  const { user, logout } = useUser();
  return (
    <nav className="flex justify-between py-6">
      <Link className="text-blue-500 font-bold text-2xl" to="/">
        LoreBlog
      </Link>

      <div className="flex items-center">
        {[Roles.Admin, Roles.Writer].includes(user?.role as any) && (
          <NavItem route="admin" />
        )}
        {user ? (
          <button
            className="bg-blue-500 h-full px-4 rounded hover:bg-blue-700"
            onClick={logout}
          >
            Logout
          </button>
        ) : (
          <NavItem route="login" />
        )}
      </div>
    </nav>
  );
};

const NavItem = ({ route }: { route: string }) => {
  const location = useLocation();
  const activeClasses = location.pathname.includes(route)
    ? "text-blue-300"
    : "";
  return (
    <Link className={`${activeClasses} underline mr-4`} to={`/${route}`}>
      {route[0].toUpperCase()}
      {route.slice(1)}
    </Link>
  );
};
