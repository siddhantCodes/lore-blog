import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { API_URL } from "../../constants";
import { User } from "../../hooks/useUser";
import { UserCard } from "../Card";

export function ManageUsers() {
  const [users, setUsers] = useState<Array<User>>([]);
  const history = useHistory();

  useEffect(() => {
    fetch(`${API_URL}/api/auth/users`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((r) => r.json())
      .then((res) => {
        if (res.ok) {
          setUsers(res.users);
        }
      });
  }, []);

  return (
    <>
      <div className="flex justify-between mb-4">
        <h2 className="text-3xl mb-4">Users</h2>
        <button
          onClick={() => history.push("/admin/users/new")}
          className="py-2 px-4 bg-blue-500 rounded"
        >
          Add User
        </button>
      </div>
      {users.map((user) => {
        return <UserCard user={user} key={user.id} />;
      })}
    </>
  );
}
