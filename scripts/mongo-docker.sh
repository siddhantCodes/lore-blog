#!/usr/bin/sh

# https://hub.docker.com/_/mongo
docker run --name lore_blog \
  -p "27017:27017" \
  -d mongo
