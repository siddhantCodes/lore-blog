import { config } from "dotenv";
import cors from "cors";
import express from "express";
import { connect as createDbConnection } from "mongoose";
import userRouter from "./api/routes/auth";
import blogsRouter from "./api/routes/blog";

config();
const app = express();
const port = process.env.PORT || 3000;

createDbConnection(
  process.env.DB_URI || "",
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    if (err) {
      console.log(err?.message);
      process.exit(1);
    }
  }
);

app.use(cors());
app.use(express.json());

app.use("/api/auth", userRouter);
app.use("/api/blogs", blogsRouter);

app.listen(port, () => {
  console.log(`App started on port: ${port}`);
});
